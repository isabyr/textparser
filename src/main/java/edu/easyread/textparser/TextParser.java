/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.easyread.textparser;

import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.StringUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author isabyr
 */
public class TextParser {
	private static TextParser parser = null;
	private Properties props;
	private StanfordCoreNLP pipeline;
	private Set<String>poses;
	
	public String parseDocument(String input){
		String paragraphs[] = input.split("\n\n");
		StringBuilder output = new StringBuilder();
		
		output.append("<span id='parser_version' style='display:none'>1.2</span>");
		
		Map<String, Integer>indexes = new HashMap<String, Integer>();
		indexes.put("s_id", 1);
		indexes.put("w_id", 1);
		
		for(String p : paragraphs){
			output.append("<p class='p_wrap'>");
			output.append(annotateParagraph(p, indexes));
			output.append("</p>");
		}
		
		return output.toString();
	}
	
	private String annotateParagraph(String paragraph, Map<String, Integer>indexes){
		StringBuilder output = new StringBuilder();
		
		Annotation document = new Annotation(paragraph);
		
		// run all Annotators on this text
		pipeline.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

		int s_counter = indexes.get("s_id");
		int w_id = indexes.get("w_id");
		int index=0;
		for (CoreMap sentence : sentences) {
		  // traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			String s_id = "<span id='s_"+(s_counter++)+"'>";
			output.append(s_id);
			for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
				// this is the text of the token
				String word = token.get(CoreAnnotations.TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
				// this is the NER label of the token
				//String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
				
				String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
				
				//String word_original = token.get(CoreAnnotations.OriginalTextAnnotation.class);
				String word_prefix = token.get(CoreAnnotations.BeforeAnnotation.class);
				//String word_postfix = token.get(CoreAnnotations.AfterAnnotation.class);
				
				String w_token = word;
				
				if(lemma.matches("[a-zA-Z0-9]+") && poses.contains(pos)){
					w_token = "<span class='span_text' id='w_"+(w_id++)+"' pos='"+pos+"' lemma='"+lemma+"'>"+word+"</span>";
				}
				
				/*System.out.println("====================================");
				System.out.println("original=="+token.get(CoreAnnotations.OriginalTextAnnotation.class)+"==");
				System.out.println("before=="+token.get(CoreAnnotations.BeforeAnnotation.class)+"==");
				//System.out.println("before=="+token.get(CoreAnnotations.)+"==");
				System.out.println("="+prefix+"=");
				System.out.println("="+TextParser.escape(prefix)+"=");
				System.out.println(word);
				output.append(TextParser.escape(prefix));.replace("\n", "<span style='display:block' />")*/
				//System.out.println("=============================");
				//System.out.println("=============================");
				output.append((word_prefix+w_token));
			}
			output.append("</span>");
		}
		indexes.put("s_id", s_counter);
		indexes.put("w_id", w_id);
		
		return output.toString();
	}
	
	private static String escape(String s) {
		StringBuilder builder = new StringBuilder();
		boolean previousWasASpace = false;
		for( char c : s.toCharArray() ) {
			if( c == ' ' ) {
				if( previousWasASpace ) {
					builder.append("&nbsp;");
					previousWasASpace = false;
					continue;
				}
				previousWasASpace = true;
			} else {
				previousWasASpace = false;
			}
			switch(c) {
				/*case '<': builder.append("&lt;"); break;
				case '>': builder.append("&gt;"); break;*/
				case '&': builder.append("&amp;"); break;
				//case '"': builder.append("&quot;"); break;
				case '\n': builder.append("<br>"); break;
				// We need Tab support here, because we print StackTraces as HTML
				case '\t': builder.append("&nbsp; &nbsp; &nbsp;"); break;  
				default:
					if( c < 128 ) {
						builder.append(c);
					} else {
						builder.append("&#").append((int)c).append(";");
					}
				//default:
				//	builder.append(c);
			}
		}
		return builder.toString();
	}
	
	
	
	
	
	
	
	private TextParser(){
		props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "invertible=true,"
				+ "tokenizeNLs=false, ptb3Escaping=false,"
				+ "americanize=false, normalizeParentheses=false,"
				+ "normalizeOtherBrackets=false,asciiQuotes=true,"
				+ "latexQuotes=false, unicodeQuotes=false,"
				+ "ptb3Dashes=false");
		pipeline = new StanfordCoreNLP(props);
		String s = "CC,CD,DT,EX,FW,IN,JJ,JJR,JJS,LS,MD,NN,NNS,NNP,NNPS,PDT,POS,PRP,PRP$,RB,RBR,RBS,RP,SYM,TO,UH,VB,VBD,VBG,VBN,VBP,VBZ,WDT,WP,WP$,WRB";
		poses = new HashSet<String>(Arrays.asList(s.split(",")));
	}
	
	public static TextParser getInstance(){
		if(parser==null){
			parser = new TextParser();
		}
		return parser;
	}
	
	
}