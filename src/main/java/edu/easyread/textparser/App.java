/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.easyread.textparser;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.ServerRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author isabyr
 */
public class App extends NanoHTTPD {

	public static void main(String args[]) throws FileNotFoundException{
		//System.out.println("-------------------");
		//String s = "Big banks and investors are making contingency plans to deal with the potential impact on the $5tn 'repo market' of the US government missing a payment on its debt.";
		//String s = "Big banks and investors are making contingency plans to deal with the potential impact on the $5tn “repo market” of the US government missing a payment on its debt.";
		//String s = "My dog also likes eating sausages.";
		TextParser parser = TextParser.getInstance();
		//parser.parseDocument2(s);
		//System.out.println(parser.parseText(s));

		ServerRunner.run(App.class);
	}

	public App() {
		super("localhost", 9090);
	}

	@Override
	public Response serve(IHTTPSession session) {
		NanoHTTPD.Method method = session.getMethod();
		String uri = session.getUri();
		System.out.println(method + " '" + uri + "' ");

		//String msg = "<html><body>";
		String msg = "";
		
		try{
			if (uri.equals("/parse/")) {
				Map<String, String> files = new HashMap<String, String>();
				session.parseBody(files);

				String postDataStr = files.get("postData");
				JSONObject postData = (JSONObject)JSONValue.parse(postDataStr);
				if(postData.containsKey("document")){
					String document = postData.get("document").toString();
					msg=TextParser.getInstance().parseDocument(document);
				}
			}
		} catch(IOException e){
			
		} catch(ResponseException e){
			
		} finally {
			if(msg.isEmpty())
				msg = "<html><body><span class='message'>Some error</span></body></html";
		}

		//msg += "</body></html>\n";

		return new NanoHTTPD.Response(msg);
	}
}
