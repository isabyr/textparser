package fi.iki.elonen;

import java.io.IOException;

public class ServerRunner {

	public static void run(Class serverClass) {
		try {
			executeInstance((NanoHTTPD) serverClass.newInstance());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void executeInstance(NanoHTTPD server) {
		try {
			server.start();
		} catch (IOException ioe) {
			System.err.println("Couldn't start server:\n" + ioe);
			System.exit(-1);
		}

		System.out.println("Server started, Hit Enter x stop.\n");

		int input = 0;
		while(input!=120){
			try{
				input = System.in.read();
			} catch(IOException ioex){}
		}

		server.stop();
		System.out.println("Server stopped.\n");
	}
}
